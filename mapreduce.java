//************************************ Map Reduce Assignments
//************************************ Assessment 1*********************************************************


/*

A-	Create files (having columns: EmpID, Name, band, subject, marks) and copy them to folder
	employee/input under your home directory in HDFS

	hadoop fs -mkdir /user/cloudera/employees/input
	hadoop fs -put Hadoop_data.txt /user/cloudera/employees/input
	hadoop fs -put OtherAssessment_data.txt /user/cloudera/employees/input 

*/


/*

B-	Create and execute a Java Map Reduce job to find total marks scored by employees in all
	assessments using employee/input as input folder and employee/totScore as output folder
*/
	
 //mapper.java
package trg.hadoop.employee;
import java.io.IOException;
import java.util.StringTokenizer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat

public class SumMapper extends Mapper<LongWritable, Text, Text , IntWritable> {
	@Override
	protected void map(LongWritable key,Text value ,Context context) throws IOException,InterruptedException {
		String[] tokens=value.toString().split(",");
		
		String emp_id;
		String name;
		String band;
		String subject;
		int total_marks=0;
		int marks;
		
		emp_id=tokens[0];
		name=tokens[1];
		band=tokens[2];
		subject=tokens[3];
		marks== Integer.valueOf(token[4]);
		
		
		context.write(new Text(emp_id), new IntWritable(marks));
		}
	}

	//reducer.java
	
	package trg.hadoop.employee;
import java.io.IOException;
import java.util.StringTokenizer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat

public class SumReducer extends Reducer<Text,IntWritable,Text,IntWritable> {
	@Override
	protected void reduce(Text empId,Iterable<IntWritable> marks_arr, Context context)
		throws IOException,InterruptedException {
			
			int sum=0;
			int i=0;
			for (IntWritable marks : marks_arr){
				sum+=marks.get();
				i++;
			}
			context.write(empId,new IntWritable(sum));
		}
}










//************************************* Assessment 2*************************************************


/* Create and execute a Java Map Reduce job to find average marks scored in assessments for
 each skill using employee/input as input folder and employee/avgScore as output folder */

 //mapper.java
package trg.hadoop.employee;
import java.io.IOException;
import java.util.StringTokenizer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat

public class AvgMapper extends Mapper<LongWritable, Text, Text , IntWritable> {
	@Override
	protected void map(LongWritable key,Text value ,Context context) throws IOException,InterruptedException {
		String[] tokens=value.toString().split(",");
		
		String emp_id;
		String name;
		String band;
		String subject;
		int total_marks=0;
		int marks;
		
		emp_id=tokens[0];
		name=tokens[1];
		band=tokens[2];
		subject=tokens[3];
		marks== Integer.valueOf(token[4]);
		
		
		context.write(new Text(emp_id), new IntWritable(marks));
		}
	}

	//reducer.java
	
	package trg.hadoop.employee;
import java.io.IOException;
import java.util.StringTokenizer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat

public class AvgReducer extends Reducer<Text,IntWritable,Text,IntWritable> {
	@Override
	protected void reduce(Text empId,Iterable<IntWritable> marks_arr, Context context)
		throws IOException,InterruptedException {
			
			int sum=0;
			int i=0;
			for (IntWritable marks : marks_arr){
				sum+=marks.get();
				i++;
			}
			context.write(empId,new IntWritable(sum/i));
		}
}



