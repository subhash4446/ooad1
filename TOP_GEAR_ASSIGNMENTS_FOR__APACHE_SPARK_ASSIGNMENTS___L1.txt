	TOP GEAR ASSIGNMENTS FOR  APACHE SPARK ASSIGNMENTS – L1  

Assignments – 1 (Creation of  RDD and  operations on RDDs )   
Note : Identify your spark context and answer the following. Wherever applicable create a new RDD.

Consider the following array.     
{10,21,90,34,40,98,21,44,59,21,90,34,29,19, 21,90,34,29,49,78 } . Create a new RDD for each of the following assignments. 

1.	For the above array,
a)	Create a RDD for the above array. 

	val rdd = sc.parallelize(List(10,21,90,34,40,98,21,44,59,21,90,34,29,19, 21,90,34,29,49,78))

b)	Display the array 
	
	rdd.collect()
	
c)	Display the first element of the array 

	rdd.first()

2.	Consider the above array
a)	Display the sorted output (ascending and descending) through an RDD. 

	ascending = rdd.sortBy(x => x,ascending = true).collect()
	descending = rdd.sortBy(x => x,ascending = false).collect()
	

b)	Display the distinct elements of the array using an RDD

	val rdd1 = rdd.distinct()
	rdd1.collect()

c)	Display distinct elements without using a new RDD.

	rdd.distinct().collect()

3.	Consider the above array 
a)	Display maximum and minimum of given array using RDD.

	minimum = rdd.min()
	maximum = rdd.max()

b)	Display top 5 list elements using RDD

	rdd.top(5)

c)	Combine above array with a new array { 30,35,45,60,75,85} and display output. 

	val rdd3 = rdd.union(rdd2)
	rdd3.collect()

d)	Provide the sum of the array elements using reduce with distinct values.

	rdd3.distinct.reduce(_+_)

e)	Provide the sum of the array elements using reduce.
	
	rdd3.reduce(_+_)